// if we are working on localhost, use the websocket setup there, otherwise use the heroku/live source
var socket = io((window.location.href === "http://localhost:3000/" ? "" : "http://fathomless-citadel-3727.herokuapp.com:80/"));

socket.on("filter-added", function (text) {
	console.log("add filter");
	// replace with a js template engine
	$("ul#filters").append("<li data-text=" + text + ">" + text +"&nbsp;&nbsp;<span class='removeFilter'>X</span></li>"); 
});
socket.on("filter-removed", function (text) {
	$("ul#filters li").each(function (index, element) {
		console.log(text);
		if (element.getAttribute("data-text") === text) {
			element.remove();
		}
	})
})
socket.on("setup-filters", function (value) {
	$("ul#filters").html('');
	value.split(",").forEach( function (item) {
		// replace with a js template engine
		$("ul#filters").append("<li data-text=" + item + ">" + item +"&nbsp;&nbsp;<span class='removeFilter'>X</span></li>"); 
	})
})

$("body").on("click", ".removeFilter", function () {
	var text = $(this).parent().attr('data-text');
	console.log('remove filter')
	socket.emit("remove-filter", text);
});
var counter = 0;
socket.on("twitter", function (tweet) {
	counter++;
	$("div#counter").html(counter);
	// replace with a js template engine
	$("ul#tweets").append("<li><strong>" + tweet.user.screen_name + "</strong>:" + tweet.text + "</li>"); 
})

socket.on("twitter-no-place", function (tweet) {
	// console.log(tweet);
	console.log("tweet no location")
	console.log(tweet);
	// replace with a js template engine
	$("ul#tweets").append("<li><strong>" + tweet.user.screen_name + "</strong>:" + tweet.text + "</li>"); 
})

// $("#submitFilter").on("click", function () {
// 	var filterInput = $("#newFilter");
// 	socket.emit("new-filter", filterInput.val());
// 	filterInput.val('');
// })

$("#addFilterForm").on("submit", function (event) {
	event.preventDefault();
	var filterInput = $("#newFilter");
	socket.emit("new-filter", filterInput.val());
	filterInput.val('');
})