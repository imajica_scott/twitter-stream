var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var port = (process.env.NODE_ENV == 'production') ? process.env.PORT : 3000;
var twit = require('twit');

var T = new twit({
  consumer_key: process.env.tw_consumer_key,
  consumer_secret: process.env.tw_consumer_secret,
  access_token: process.env.tw_access_token_key,
  access_token_secret: process.env.tw_access_token_secret
});

app.get('/', function (req, res) {
	res.sendFile(__dirname + '/index.html');
});

// This isn't the correct way to do this...
app.get('/css/styles.css', function (req, res) {
	res.sendFile(__dirname + '/css/styles.css');
});
app.get('/js/app.js', function (req, res) {
	res.sendFile(__dirname + '/js/app.js');
});

var streaming = false;
var filters = ['bananas', 'oranges', 'strawberries']
var theStream;
io.on('connection', function (socket) {
	// setup the stream to constantly run
	io.emit('setup-filters', filters.toString());
	if (!streaming) { // so we dont setup multiple connections to the same stream, start stream on first client connect
		streaming = true;
		console.log("setting up new stream")
		createStream(filters);
	}
	socket.on("new-filter", function (text) {
		console.log("adding new filter: %s", text);
		filters.push(text);
		createStream(filters);
		io.emit("filter-added", text);
	}) 

	socket.on("remove-filter", function (text) {
		console.log("removing filter: %s", text);
		filters.splice(filters.indexOf(text), 1); // splice it out the array
		createStream(filters);
		io.emit("filter-removed", text);
	})
})

function createStream(filters) {
	console.log("creating stream with filter: %s", filters);
	if (theStream) {
		theStream.stop();
		console.log('stopping existing stream');
	}
	theStream = T.stream('statuses/filter', { track: filters, language : 'en' }); 
	theStream.on('tweet', function (tweet) {
		if (tweet.place) {
			console.log('tweet! %s', tweet.id);
			io.emit('twitter', tweet);
		} else {
			console.log("tweet without a place...%s", tweet.id)
			io.emit('twitter-no-place', tweet)
		}
	});
	return;
}

http.listen(port, function (){
	console.log('listening on *:' + port);
})