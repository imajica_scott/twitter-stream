This is a learning/experimental app to integrate with the twitter 'streaming' api

Please note you will need to setup an env.json file with an object "twitter", that contains your twitter consumer key, consumer secret, access token key and access token secret. Mine have been removed from the project for security purposes.

#Build Stages#

* connect and setup web socket connections - done
* connect and authorise with twitter api - done
* print out specific data from the twitter api - done
##stage 1 complete##
* allow user customisation of the data - done
* connect to specific server - done
##on hold##

* seperate namespaces enabling one to one chat with an admin
* admin can add new items/classes that broadcast and notify other clients
* handle stopping of the stream with no clients

##off hold##
* pull geolocation data out of the stream
* setup leaflet/mapbox or google maps, though less likely
* handle the stream events and print them out on the map
##stage 2 complete##
* build functionality for different namespaces with socket.io
* integrate each namespace with a specific user session (may be quite intensive)
* would it be better to build the streaming api with twitter directly into the browser with javascript?
##stage 3 complete##


##additional notes/considerations##
* limit to number of filters on the twitter stream?
* some kind of retweet exclusion?
* better default filter...
* do some fancier css handling on the new tweets feed/getting rid of old ones (likely implement a js template engine or possibly angular, for easier porting into ionic)
* handle storing of tweets so that when a connection is made, it can instantly display already fetched tweets